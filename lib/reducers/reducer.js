"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.initialData = undefined;

var _actions = require("../actions/actions");

var _actions2 = _interopRequireDefault(_actions);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _qu4rtet$require = qu4rtet.require("redux-actions"); // Copyright (c) 2018 SerialLab Corp.
//
// GNU GENERAL PUBLIC LICENSE
//    Version 3, 29 June 2007
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


const handleActions = _qu4rtet$require.handleActions;

var _qu4rtet$require2 = qu4rtet.require("./lib/reducer-helper");

const setServerState = _qu4rtet$require2.setServerState;

var _qu4rtet$require3 = qu4rtet.require("./lib/message");

const showMessage = _qu4rtet$require3.showMessage;

var _qu4rtet$require4 = qu4rtet.require("./plugins/pluginRegistration");

const pluginRegistry = _qu4rtet$require4.pluginRegistry;
const initialData = exports.initialData = () => {
  return {};
};

exports.default = handleActions({}, {});