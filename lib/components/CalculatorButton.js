"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
// Copyright (c) 2018 SerialLab Corp.
//
// GNU GENERAL PUBLIC LICENSE
//    Version 3, 29 June 2007
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

const React = qu4rtet.require("react");
const Component = React.Component;

var _qu4rtet$require = qu4rtet.require("react-router-dom");

const withRouter = _qu4rtet$require.withRouter;


class _CalculatorButton extends Component {
  static get PLUGIN_COMPONENT_NAME() {
    return "GTINCalculatorButton";
  }
  render() {
    return React.createElement("button", {
      className: "pt-button pt-icon-calculator",
      onClick: e => {
        this.props.history.push("/calculator/gtin");
      }
    });
  }
}

const CalculatorButton = exports.CalculatorButton = withRouter(_CalculatorButton);