"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.GTINCalculator = undefined;

var _ndcToGtin = require("../lib/ndcToGtin");

const React = qu4rtet.require("react"); // Copyright (c) 2018 SerialLab Corp.
//
// GNU GENERAL PUBLIC LICENSE
//    Version 3, 29 June 2007
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

var _qu4rtet$require = qu4rtet.require("./components/layouts/Panels");

const RightPanel = _qu4rtet$require.RightPanel;
const Component = React.Component;

var _qu4rtet$require2 = qu4rtet.require("react-intl");

const FormattedMessage = _qu4rtet$require2.FormattedMessage;

var _qu4rtet$require3 = qu4rtet.require("@blueprintjs/core");

const Card = _qu4rtet$require3.Card;

var _qu4rtet$require4 = qu4rtet.require("./plugins/pluginRegistration");

const pluginRegistry = _qu4rtet$require4.pluginRegistry;
class GTINCalculator extends Component {
  constructor(props) {
    super(props);

    this.calculateGtin = evt => {
      const gtins = Array(10).fill().map((_, i) => {
        let converted = (0, _ndcToGtin.NDCtoGTIN14)(evt.target.value, i);
        if (converted.length === 14) {
          return { indicator: i, value: (0, _ndcToGtin.NDCtoGTIN14)(evt.target.value, i) };
        } else {
          return { indicator: i, value: "" };
        }
      });
      this.setState({ GTINs: gtins });
    };

    this.state = { ndc: null, GTINs: [] };
  }

  render() {
    return React.createElement(
      RightPanel,
      {
        key: "GTINCalculatorPanel",
        title: React.createElement(FormattedMessage, { id: "plugins.gtinCalculator.GTINCalculatorTitle" }) },
      React.createElement(
        "div",
        { className: "calculator-cards-container" },
        React.createElement(
          Card,
          null,
          React.createElement(
            "h5",
            null,
            React.createElement(FormattedMessage, { id: "plugins.gtinCalculator.GTINCalculatorTitle" })
          ),
          React.createElement("input", {
            className: "pt-input pt-fill",
            onChange: this.calculateGtin,
            placeholder: pluginRegistry.getIntl().formatMessage({ id: "plugins.gtinCalculator.calcPlaceholder" })
          })
        ),
        React.createElement(
          Card,
          null,
          React.createElement(
            "h5",
            null,
            React.createElement(FormattedMessage, { id: "plugins.gtinCalculator.GTINs" })
          ),
          React.createElement(
            "table",
            {
              className: "pt-table pt-fill pt-striped",
              style: { width: "100%" } },
            React.createElement(
              "tbody",
              null,
              this.state.GTINs.map(item => {
                return React.createElement(
                  "tr",
                  null,
                  React.createElement(
                    "td",
                    { style: { fontSize: "16px", fontWeight: "bold" } },
                    React.createElement(FormattedMessage, {
                      id: "plugins.gtinCalculator.indicatorDigit",
                      values: { indicator: item.indicator }
                    })
                  ),
                  React.createElement(
                    "td",
                    null,
                    item.value
                  )
                );
              })
            )
          )
        )
      )
    );
  }
}
exports.GTINCalculator = GTINCalculator;